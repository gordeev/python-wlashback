# python-wlashback

**Wlashback** is module that helps you to make backup your local projects 
and MySQL database for python 3.x.

You should create settings file like a [wlashback.cfg](example/wlashback.cfg)
and set path to it


## Install


Download python wheel and install using pip
```bash
pip install wlashback-0.1-py2.py3-none-any.whl

```


## Usage

Simple example for usage in your python code:

```python
import os
from wlashback import wlashback

path = os.path.join(os.path.dirname(__file__), 'wlashback.cfg')

bup = wlashback.backup(path)
print(bup)
```