#!/usr/bin/env python

from setuptools import setup


setup(name='wlashback',
      version='0.1',
      description='''
         Wlashback is module that helps you to make backup your local projects
            and MySQL database for python 3.x.

          ''',
      author='Gordeev Andrey (washwash)',
      author_email='gordeev.and.and@gmail.com',
      url='https://github.com/washwash/python-wlashback',
      packages=['wlashback', ],
      )

