# -*- coding: utf-8 -*-

"""Wlashback 1.0
Wlashback is module for backup local projects and MySQL database
for python versions over 3.

You should create settings file like a wlashback.cfg
and set path

@author: Gordeev Andrey <gordeev.and.and@gmail.com>
"""

import os
import time
import tempfile
import tarfile
from shutil import copyfile
from configparser import ConfigParser


__all__ = ['backup', ]


# timestamp template
ts_now = '%H:%M %d.%m.%Y'
ts_directory = '%d%m%Y_%H%M'
timestamp = lambda option: time.strftime(option)

related_keys = {
    'settings': {'key': 'wlashconf'},
    'database': {'key': 'database',
                 'ms': 'dbms',
                 'parameters': [
                     'name',
                     'user',
                     'password',
                     'host',
                     'port'], },
    'output': {'key': 'backup_output', },
    'input': {'key': 'backup_input', },
}


class DataBase(object):
    def __init__(self, conf, *args, **kwargs):
        self.conf = conf

    def handler(self):
        if self.conf.get('dbms') == 'mysql':
            return MySQLHandler(self.conf)


class MySQLHandler(object):

    def __init__(self, conf):
        self.dump_code = '-1'
        for k, v in conf.items():
            setattr(self, k, v)

    def get_dump(self, path):
        path += '.sql'
        command = ' '.join(
            ['mysqldump',
             '-u {username}'.format(username=getattr(self, 'user')),

             ('-p \'{passwd}\''.format(passwd=self.password)) if
                hasattr(self, 'password') else '',

             ('-h {host}'.format(host=self.host)) if
                hasattr(self, 'host') else '',

             ('-P {port}'.format(port=self.port)) if
                hasattr(self, 'port') else '',

             '{name}'.format(name=getattr(self, 'name')),
             '> %s' % path])

        self.dump_code = os.system(command)
        return path


class Directory(object):
    def __init__(self, path, *args, **kwargs):
        self._path = path[:-1] if path[-1] == '/' else path

    @property
    def path(self):
        return self._path

    @property
    def name(self):
        return os.path.basename(self.path)


class InputDirectory(Directory):
    pass


class OutputDirectory(Directory):
    _full_path = None

    @property
    def full_path(self):
        return self._full_path

    @full_path.setter
    def full_path(self, path):
        self._full_path = path


class Section(object):
    status_success = 'success'
    status_fail = 'fail'
    def __init__(self, name, input_dirs=None, output_dirs=None, db=None):
        self._status = self.status_fail
        self.name = name
        self._inputs = list()
        self._outputs = list()
        self._db = list()

        mapper = lambda conf: _field.append(_class(conf))
        a = ((input_dirs, self._inputs, InputDirectory),
             (output_dirs, self._outputs, OutputDirectory),
             (db, self._db, DataBase), )
        for _arg, _field, _class in a:
            list(map(mapper, _arg))

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    @property
    def outputs(self):
        return self._outputs

    @property
    def inputs(self):
        return self._inputs

    @property
    def db(self):
        for db in self._db:
            yield db.handler()


class Wlashback(object):
    settings_key = related_keys['settings']['key']
    output_key = related_keys['output']['key']
    input_key = related_keys['input']['key']
    db_key = related_keys['database']['key']
    db_params = related_keys['database']['parameters']
    dbms_key = related_keys['database']['ms']

    def __init__(self, config):
        self.start_time = timestamp(ts_now)
        self.finish_time = timestamp(ts_now)
        self.start_time_dir = timestamp(ts_directory)
        self.config = config
        self._settings = None
        self._sections = list()

    def status(self):
        return {'start': self.start_time,
                'finish': self.finish_time,
                'sections': {str(x): x.status for x in self.sections}, }

    @property
    def sections(self):
        return self._sections

    @sections.setter
    def sections(self, section):
        self._sections.append(section)

    def parse(self):
        for section in self.config.sections():
            init = {'db': [],
                    'out_dirs': '',
                    'in_dirs': '', }
            curr_section = self.config[section]
            if curr_section.name == self.settings_key:
                self._settings = curr_section.items()
                continue
            if curr_section.get(self.output_key):
                for key, config in curr_section.items():
                    if key == self.output_key:
                        init['out_dirs'] = \
                            [x for x in curr_section.get(key).split()]
                    elif key == self.input_key:
                        init['in_dirs'] = \
                            [x for x in curr_section.get(key).split()]
                    elif key.startswith(self.db_key):
                        # TODO: catch the split() exception
                        rawdb = config.split()
                        dbconf = {self.dbms_key: key.split('__')[1]}

                        for cset in rawdb:
                            name, value = cset.split('=')
                            if name in self.db_params:
                                dbconf[name] = value

                        init['db'].\
                            append(dbconf)
                section = Section(name=curr_section.name,
                                  input_dirs=init['in_dirs'],
                                  output_dirs=init['out_dirs'],
                                  db=init['db'])
                self.sections = section
            else:
                raise AttributeError('Check fields in config file')

    def backup(self, section):
        for dout in section.outputs:
            dout.full_path = os.path.join(
                dout.path,
                self.start_time_dir)
            if not os.path.exists(dout.full_path):
                os.makedirs(dout.full_path)

        with tempfile.TemporaryDirectory(prefix='wlb_') as wb_container:
            for din in section.inputs:
                tar_name = os.path.join(wb_container, din.name)
                with tarfile.open(tar_name, 'w:gz') as tar:
                    tar.add(din.path,
                            arcname=os.path.basename(din.path),
                            recursive=True)
                for dout in section.outputs:
                    copyfile(tar_name,
                             os.path.join(dout.full_path,
                                          'source.tar.gz'))

            with tempfile.TemporaryDirectory(
                    prefix='database_', dir=wb_container) as db_dir:
                for db in section.db:
                    tar_name = os.path.join(wb_container, db.name)
                    with tarfile.open(tar_name, 'w:gz') as tar:
                        sql_path = db.get_dump(os.path.join(db_dir, db.name))
                        tar.add(sql_path, arcname=db.name, recursive=True)
                    for dout in section.outputs:
                        copyfile(
                            tar_name,
                            os.path.join(
                                dout.full_path,
                                'database__{}.tar.gz'.format(db.name)))

        section.status = Section.status_success

    def action(self):
        self.parse()
        list(map(self.backup, self.sections))
        self.finish_time = timestamp(ts_now)


def backup(config_path):
    config = ConfigParser()
    if config.read(config_path):
        wb = Wlashback(config)
        wb.action()
        return wb.status()
    else:
        raise IOError('missing .cfg file!')
